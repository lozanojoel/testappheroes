//
//  ViewController.swift
//  TestPp
//
//  Created by Joel Lozano on 10/25/19.
//  Copyright © 2019 Joel Lozano. All rights reserved.
//

import UIKit

class ViewController: UIViewController  {
    
    var superHeroesCollection = [superheroes]()
    var estimaWith = 190.0
    var cellMarginSize  = 10.0
    var heroDetail : superheroes?
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.Data()
        
        // Register Cells
        self.collectionView.register(UINib(nibName: "ItemCell", bundle: nil), forCellWithReuseIdentifier: "ItemCell")
    }
    
    func Data() {
        let jsonUrlString = "https://api.myjson.com/bins/bvyob"
        guard  let url = URL(string: jsonUrlString) else {
            return
        }

        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, err) in
            guard let data = data else {return}
            
            do {
                let heroe = try JSONDecoder().decode(listSH.self, from: data)
               
                for i in heroe.superheroes{
                    self.superHeroesCollection.append(i)
                }
                
            }catch let jsonErr {
                print("error serialiation JSon", jsonErr)
            }
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            }).resume()
        
        
    }

}


extension ViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return Lista.count
        return superHeroesCollection.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCell", for: indexPath) as! CellCollectionView
        
        let img = superHeroesCollection[indexPath.row].photo
        let heroName = superHeroesCollection[indexPath.row].name
        
        
        let url = URL(string: img)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            DispatchQueue.main.async() {
                cell.setData(heroImage: UIImage(data: data)! , name: heroName)
            }
        }
        
        task.resume()
        return cell
}
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        heroDetail = superHeroesCollection[indexPath.row]
        performSegue(withIdentifier: "showDetail", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail"{
            let vc = segue.destination as! DetailHeroesViewController
            vc.heroe = heroDetail
        }
    }
}


extension ViewController: UICollectionViewDelegateFlowLayout{
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let with = self.calculateWith()
        return CGSize(width: with, height: with)
        
    }
    
    func calculateWith() -> CGFloat{
        let estimsateWith = CGFloat(estimaWith)
        let cellCount = floor(CGFloat(self.view.frame.size.width / estimsateWith))
        
        let margin = CGFloat(cellMarginSize * 2)
        let with = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
        return with
    }
}

