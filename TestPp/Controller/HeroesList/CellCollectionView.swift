//
//  CellCollectionView.swift
//  TestPp
//
//  Created by Joel Lozano on 10/25/19.
//  Copyright © 2019 Joel Lozano. All rights reserved.
//

import UIKit

class CellCollectionView: UICollectionViewCell {
    
    @IBOutlet weak var imageHero: UIImageView!
    @IBOutlet weak var nameHero: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setData(heroImage: UIImage, name: String){
        imageHero.image = heroImage
        nameHero.text = name
    }

    
}
