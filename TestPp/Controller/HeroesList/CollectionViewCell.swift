//
//  CollectionViewCell.swift
//  TestPp
//
//  Created by Joel Lozano on 10/25/19.
//  Copyright © 2019 Joel Lozano. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var name: UILabel!
}
