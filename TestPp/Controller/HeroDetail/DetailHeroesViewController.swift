//
//  DetailHeroesViewController.swift
//  TestPp
//
//  Created by Joel Lozano on 10/28/19.
//  Copyright © 2019 Joel Lozano. All rights reserved.
//

import UIKit

class DetailHeroesViewController: UIViewController {
    
    var heroe  : superheroes?

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var realName: UILabel!
    @IBOutlet weak var abilities: UILabel!
    @IBOutlet weak var power: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var groups: UILabel!
    

    override func viewDidLoad() {
           super.viewDidLoad()
        
        let url = URL(string: heroe!.photo)
        let task = URLSession.shared.dataTask(with: url!, completionHandler: { data, response, error in
            guard let data = data, error == nil else {
                return
            }
            self.photo.image = UIImage(data: data)
            })
    
        task.resume()
        
        name.text = heroe?.name
        realName.text = heroe?.realName
        abilities.text = heroe?.abilities
        power.text = heroe?.power
        height.text = heroe?.height
        groups.text = heroe?.groups
        
       }


}
