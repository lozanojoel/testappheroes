//
//  SuperHeroes.swift
//  TestPp
//
//  Created by Joel Lozano on 10/25/19.
//  Copyright © 2019 Joel Lozano. All rights reserved.
//

import Foundation

struct listSH : Decodable {
    var superheroes : [superheroes]
}

struct superheroes : Decodable {
    var name: String
    var photo: String
    var realName: String
    var height: String
    var power: String
    var abilities: String
    var groups: String
    
}
